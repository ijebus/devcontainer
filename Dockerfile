FROM golang:1

ARG INSTALL_ZSH="true"
ARG UPGRADE_PACKAGES="true"
# ARG ENABLE_NONROOT_DOCKER="true"
# ARG SOURCE_SOCKET=/var/run/docker-host.sock
# ARG TARGET_SOCKET=/var/run/docker.sock

ARG USERNAME=vscode
ARG USER_UID=1000
ARG USER_GID=$USER_UID

ARG COMMON_SCRIPT_SOURCE="https://raw.githubusercontent.com/microsoft/vscode-dev-containers/master/script-library/common-debian.sh"
ARG COMMON_SCRIPT_SHA="dev-mode"
# ARG DOCKER_SCRIPT_SOURCE="https://raw.githubusercontent.com/microsoft/vscode-dev-containers/master/script-library/docker-debian.sh"
# ARG DOCKER_SCRIPT_SHA="dev-mode"
# ARG NVM_YARN_SCRIPT_SOURCE="https://raw.githubusercontent.com/microsoft/vscode-dev-containers/master/script-library/node-debian.sh"
# ARG NVM_YARN_SCRIPT_SHA="dev-mode"

ENV DEBIAN_FRONTEND=noninteractive
# NVM_DIR=/usr/local/share/nvm \
# NVM_SYMLINK_CURRENT=true \ 
# PATH=${NVM_DIR}/current/bin:${PATH}


RUN apt-get update \
    && apt-get -y install --no-install-recommends curl ca-certificates 2>&1 \
    #
    # Common
    && curl -sSL  ${COMMON_SCRIPT_SOURCE} -o /tmp/common-setup.sh \
    && ([ "${COMMON_SCRIPT_SHA}" = "dev-mode" ] || (echo "${COMMON_SCRIPT_SHA} */tmp/common-setup.sh" | sha256sum -c -)) \
    && /bin/bash /tmp/common-setup.sh "${INSTALL_ZSH}" "${USERNAME}" "${USER_UID}" "${USER_GID}" "${UPGRADE_PACKAGES}" \
    #
    # Docker
    # && curl -sSL ${DOCKER_SCRIPT_SOURCE} -o /tmp/docker-setup.sh \
    # && ([ "${DOCKER_SCRIPT_SHA}" = "dev-mode" ] || (echo "${DOCKER_SCRIPT_SHA} */tmp/docker-setup.sh" | sha256sum -c -)) \
    # && /bin/bash /tmp/docker-setup.sh "${ENABLE_NONROOT_DOCKER}" "${SOURCE_SOCKET}" "${TARGET_SOCKET}" "${USERNAME}" \
    #
    # Node
    # && rm -rf /opt/yarn-* /usr/local/bin/yarn /usr/local/bin/yarnpkg \
    # && curl -sSL ${NVM_YARN_SCRIPT_SOURCE} -o /tmp/nvm-setup.sh \
    # && ([ "${NVM_YARN_SCRIPT_SHA}" = "dev-mode" ] || (echo "${NVM_YARN_SCRIPT_SHA} */tmp/nvm-setup.sh" | sha256sum -c -)) \
    # && /bin/bash /tmp/nvm-setup.sh "${NVM_DIR}" "none" "${USERNAME}" \
    #
    # Cleanup
    && apt-get autoremove -y \
    && apt-get clean -y \
    && rm -rf /var/lib/apt/lists/* /tmp/docker-setup.sh /tmp/common-setup.sh /tmp/nvm-setup.sh /root/.gnupg

# Install the remaining needfuls
RUN \
    # Install Starship Prompt
    curl -sSfL https://starship.rs/install.sh | bash -s -- -y -b /usr/local/bin 2>&1 \
    && sh -c 'echo eval "$(starship init zsh)"' >> /home/"${USERNAME}"/.zshrc \
    #
    # Install Postgres CLI
    # && sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt/ `lsb_release -cs`-pgdg main" >> /etc/apt/sources.list.d/pgdg.list' \
    # && wget -q https://www.postgresql.org/media/keys/ACCC4CF8.asc -O - | apt-key add - \
    # && apt-get update \
    # && apt-get -y install postgresql-client-12 \
    #
    # Build Go tools w/module support
    && mkdir -p /tmp/gotools \
    && cd /tmp/gotools \
    && GOPATH=/tmp/gotools GO111MODULE=on go get -v golang.org/x/tools/gopls@latest 2>&1 \
    && GOPATH=/tmp/gotools GO111MODULE=on go get -v \
    honnef.co/go/tools/...@latest \
    golang.org/x/tools/cmd/gorename@latest \
    golang.org/x/tools/cmd/goimports@latest \
    golang.org/x/tools/cmd/guru@latest \
    golang.org/x/lint/golint@latest \
    github.com/mdempsky/gocode@latest \
    github.com/cweill/gotests/...@latest \
    github.com/haya14busa/goplay/cmd/goplay@latest \
    github.com/sqs/goreturns@latest \
    github.com/josharian/impl@latest \
    github.com/davidrjenni/reftools/cmd/fillstruct@latest \
    github.com/uudashr/gopkgs/v2/cmd/gopkgs@latest  \
    github.com/ramya-rao-a/go-outline@latest  \
    github.com/acroca/go-symbols@latest  \
    github.com/godoctor/godoctor@latest  \
    github.com/rogpeppe/godef@latest  \
    github.com/zmb3/gogetdoc@latest \
    github.com/fatih/gomodifytags@latest  \
    github.com/mgechev/revive@latest  \
    github.com/go-delve/delve/cmd/dlv@latest 2>&1 \
    #
    # Build Go tools w/o module support
    && GOPATH=/tmp/gotools go get -v github.com/alecthomas/gometalinter 2>&1 \
    #
    # Build gocode-gomod
    && GOPATH=/tmp/gotools go get -x -d github.com/stamblerre/gocode 2>&1 \
    && GOPATH=/tmp/gotools go build -o gocode-gomod github.com/stamblerre/gocode \
    #
    # Install Go tools
    && mv /tmp/gotools/bin/* /usr/local/bin/ \
    && mv gocode-gomod /usr/local/bin/ \
    #
    # Install golangci-lint
    && curl -sSfL https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh | sh -s -- -b /usr/local/bin 2>&1 \
    #
    # Cleanup
    && apt-get autoremove -y \
    && apt-get clean -y \
    && rm -rf /var/lib/apt/lists/* /tmp/gotools

USER vscode

ENV DEBIAN_FRONTEND=dialog \
    GO111MODULE=on

# ENTRYPOINT [ "/usr/local/share/docker-init.sh" ]

CMD [ "sleep", "infinity" ]
