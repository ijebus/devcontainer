# .devcontainer

I've been enjoying [VSCode](https://code.visualstudio.com/) and in particular some of the [Remote Development](https://code.visualstudio.com/docs/remote/remote-overview) extensions. Especially so, the Development containers.

The problem is, for each project I start I often find myself adding the same tools over and over to the template/builtin devcontainers.

This project is my personal starter devcontainer. Tools/languages that I use all the time are included by default, while often but less frequently used are available but commented out. Consider this a work-in-progress!

It does lean heavily on those templates provided by Microsoft, so thanks to them for those.
